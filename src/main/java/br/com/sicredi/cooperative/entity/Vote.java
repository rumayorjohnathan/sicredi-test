package br.com.sicredi.cooperative.entity;

import java.io.Serializable;

import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@RedisHash("Vote")
public class Vote implements Serializable {

	private static final long serialVersionUID = 3038303424662853804L;

	@Indexed
	private Long id;

	@Indexed
	private Long sessionId;

	@Indexed
	private Long partnerId;
	private Long creation;
	private Boolean value;

	public Vote() {
		super();
	}

	public Vote(Long id, Long sessionId, Long partnerId, Long creation, Boolean value) {
		this();
		this.id = id;
		this.sessionId = sessionId;
		this.partnerId = partnerId;
		this.creation = creation;
		this.value = value;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	public Long getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(Long partnerId) {
		this.partnerId = partnerId;
	}

	public Long getCreation() {
		return creation;
	}

	public void setCreation(Long creation) {
		this.creation = creation;
	}

	public Boolean getValue() {
		return value;
	}

	public void setValue(Boolean value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Vote [id=" + id + ", sessionId=" + sessionId + ", partnerId=" + partnerId + ", creation=" + creation
				+ ", value=" + value + "]";
	}

}
