package br.com.sicredi.cooperative.entity;

import java.io.Serializable;

import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@RedisHash("Partner")
public class Partner implements Serializable {

	private static final long serialVersionUID = -5954370938792289193L;

	@Indexed
	private Long id;

	@Indexed
	private String cpf;

	public Partner() {
		super();
	}

	public Partner(Long id, String cpf) {
		this();
		this.id = id;
		this.cpf = cpf;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	@Override
	public String toString() {
		return "Partner [id=" + id + ", cpf=" + cpf + "]";
	}

}
