package br.com.sicredi.cooperative.api.v1;

import java.io.Serializable;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.sicredi.cooperative.api.v1.output.ResponseOutput;
import br.com.sicredi.cooperative.util.SicrediBusinessException;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ApiResponses(value = {
		@ApiResponse(code = 200, message = "OK.", response = ResponseOutput.class),
		@ApiResponse(code = 201, message = "Created.", response = ResponseOutput.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ResponseOutput.class),
		@ApiResponse(code = 404, message = "Not Found", response = ResponseOutput.class),
		@ApiResponse(code = 409, message = "Conflict", response = ResponseOutput.class),
		@ApiResponse(code = 422, message = "Unprocessable Entity.", response = ResponseOutput.class),
		@ApiResponse(code = 500, message = "Internal Server Error", response = ResponseOutput.class)
})
public abstract class ApiBase implements Serializable {

	private static final long serialVersionUID = -8060759718288118627L;

	@Autowired
	private Logger logger;

	@ExceptionHandler({ SicrediBusinessException.class })
	public ResponseEntity<?> handleSicrediBusinessException(SicrediBusinessException sbe) {
		final ResponseOutput<?> response = new ResponseOutput<Serializable>(sbe.getMessage());
		logger.info(StringUtils.join("checked error response payload for createDiscussion -> ", response));

		return ResponseEntity.status(sbe.getHttpStatus()).body(response);
	}

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<?> handleException(Exception e) {
		final ResponseOutput<?> response = new ResponseOutput<Serializable>(e.getMessage());
		logger.error(StringUtils.join("error response payload for createSession -> ", response));

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
	}

	ResponseEntity<?> errorsResponse(BindingResult result) {
		final ResponseOutput<Serializable> response = new ResponseOutput<Serializable>(
				result
				.getAllErrors()
				.stream()
				.map(ObjectError::getDefaultMessage)
				.collect(Collectors.toList())
				);

		logger.info(StringUtils.join("payload errors for response -> ", response));

		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(response);
	}

}
