package br.com.sicredi.cooperative.api.v1;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sicredi.cooperative.api.v1.input.VoteInput;
import br.com.sicredi.cooperative.api.v1.output.ResponseOutput;
import br.com.sicredi.cooperative.dto.KeyDTO;
import br.com.sicredi.cooperative.dto.VoteDTO;
import br.com.sicredi.cooperative.service.VoteService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/v1/discussions")
public class SessionVoteApi extends ApiBase {

	private static final long serialVersionUID = -112893215845287402L;

	@Autowired
	private Logger logger;

	@Autowired
	private VoteService voteService;

	@Autowired
	private ModelMapper modelMapper;

	@ApiOperation(value = "Realiza a criação de votos para determinada sessão de discussão. "
			+ "Uma sessão de discussão deve estar cadastrada e aberta para possibilidade de voto.")
	@PostMapping(path = "/{discussionId}/votes", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createVote(@PathVariable Long discussionId, @RequestBody @Valid VoteInput vote, BindingResult result) {
		logger.info(StringUtils.join("Input id for createVote -> ", discussionId));

		if (result.hasErrors()) {
			return errorsResponse(result);
		}

		final ResponseOutput<KeyDTO> response = new ResponseOutput<KeyDTO>(voteService.createVote(discussionId, modelMapper.map(vote, VoteDTO.class)));

		logger.info(StringUtils.join("response payload id for createVote -> ", response));

		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}
}
