package br.com.sicredi.cooperative.api.v1.input;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.sicredi.cooperative.util.CPFDeserializer;
import br.com.sicredi.cooperative.util.JsonUtils;

public class VoteInput implements Serializable {

	private static final long serialVersionUID = -3894047940451476583L;

	private Boolean value;
	private String cpf;

	public VoteInput() {
		super();
	}

	public VoteInput(Boolean value, String cpf) {
		this();
		this.value = value;
		this.cpf = cpf;
	}

	@NotNull(message = "Voto do cooperado deve ser informado.")
	public Boolean getValue() {
		return value;
	}

	public void setValue(Boolean value) {
		this.value = value;
	}

	@JsonDeserialize(using = CPFDeserializer.class)
	@CPF
	@NotBlank(message = "CPF do cooperado deve ser informado.")
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	@Override
	public String toString() {
		return JsonUtils.logJson(this);
	}

}
