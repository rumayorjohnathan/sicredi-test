package br.com.sicredi.cooperative.api.v1.input;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.sicredi.cooperative.util.JsonUtils;
import br.com.sicredi.cooperative.util.LocalDateTimeDeserializer;
import br.com.sicredi.cooperative.util.NonPastDate;

public class SessionInput implements Serializable {

	private static final long serialVersionUID = -4989950566401356814L;

	private LocalDateTime deadline;

	public SessionInput() {
		super();
	}

	public SessionInput(LocalDateTime created, LocalDateTime deadline, Boolean open, Long discussionId) {
		this();
		this.deadline = deadline;
	}

	@NonPastDate
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	public LocalDateTime getDeadline() {
		return deadline;
	}

	public void setDeadline(LocalDateTime deadline) {
		this.deadline = deadline;
	}

	@Override
	public String toString() {
		return JsonUtils.logJson(this);
	}

}
