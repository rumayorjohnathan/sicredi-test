package br.com.sicredi.cooperative.api.v1.input;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import br.com.sicredi.cooperative.util.JsonUtils;

public class DiscussionInput implements Serializable {

	private static final long serialVersionUID = 7007395798695593123L;

	private String title;
	private String description;

	public DiscussionInput() {
		super();
	}

	public DiscussionInput(String title, String description, String creator) {
		this();
		this.title = title;
		this.description = description;
	}

	@NotBlank(message = "Título da Pauta deve ser preenchido.")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@NotBlank(message = "Descrição da Pauta deve ser preenchida.")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return JsonUtils.logJson(this);
	}

}
