package br.com.sicredi.cooperative.api.v1;

import java.time.ZoneOffset;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sicredi.cooperative.api.v1.input.SessionInput;
import br.com.sicredi.cooperative.api.v1.output.ResponseOutput;
import br.com.sicredi.cooperative.dto.KeyDTO;
import br.com.sicredi.cooperative.dto.SessionDTO;
import br.com.sicredi.cooperative.service.SessionService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/v1/discussions")
public class DiscussionSessionApi extends ApiBase {

	private static final long serialVersionUID = -3119940650598397652L;

	@Autowired
	private Logger logger;

	@Autowired
	private SessionService sessionService;

	@ApiOperation(value = "Realiza a criação de nova sessão de votação para determinada discussão. "
			+ "Uma discussão deve estar cadastrada para possibilidade criação de sessão.")
	@PostMapping(path = "/{discussionId}/sessions", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createSession(@PathVariable Long discussionId, @RequestBody @Valid SessionInput session, BindingResult result) {
		logger.info(StringUtils.join("Input id for createSession -> ", discussionId));

		if (result.hasErrors()) {
			return errorsResponse(result);
		}

		final SessionDTO sessionDto = new SessionDTO();
		final Long deadline = session.getDeadline() == null ? null : session.getDeadline().atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli();
		sessionDto.setDeadline(deadline);
		final ResponseOutput<KeyDTO> response = new ResponseOutput<KeyDTO>(sessionService.createSession(discussionId, sessionDto));
		logger.info(StringUtils.join("response payload for createSession -> ", response));

		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	@ApiOperation(value = "Realiza a busca de determinada sessão de votação para discussão.")
	@GetMapping(path = "/{discussionId}/sessions", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findSession(@PathVariable Long discussionId) {
		logger.info(StringUtils.join("Input id for createSession -> ", discussionId));

		final ResponseOutput<SessionDTO> response = new ResponseOutput<SessionDTO>(sessionService.findSessionSummaryByDiscussionId(discussionId));
		logger.info(StringUtils.join("response payload for createSession -> ", response));

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
}
