package br.com.sicredi.cooperative.api.v1;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sicredi.cooperative.api.v1.input.DiscussionInput;
import br.com.sicredi.cooperative.api.v1.output.ResponseOutput;
import br.com.sicredi.cooperative.dto.DiscussionDTO;
import br.com.sicredi.cooperative.dto.KeyDTO;
import br.com.sicredi.cooperative.service.DiscussionService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/v1/discussions")
public class DiscussionApi extends ApiBase {

	private static final long serialVersionUID = -1879885598851450116L;

	@Autowired
	private Logger logger;

	@Autowired
	private DiscussionService discussionService;

	@Autowired
	private ModelMapper modelMapper;

	@ApiOperation(value = "Realiza a criação de nova discussão.")
	@PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createDiscussion(@RequestBody @Valid DiscussionInput discussion, BindingResult result) {
		logger.info(StringUtils.join("Input payload for createDiscussion -> ", discussion));

		if (result.hasErrors()) {
			return errorsResponse(result);
		}

		final ResponseOutput<KeyDTO> response = new ResponseOutput<KeyDTO>(discussionService.createDiscussion(modelMapper.map(discussion, DiscussionDTO.class)));
		logger.info(StringUtils.join("response payload for createDiscussion -> ", response));

		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	@ApiOperation(value = "Realiza busca de determinada discussão.")
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findDiscussion(@PathVariable Long id) {
		logger.info(StringUtils.join("Input id for findDiscussion -> ", String.valueOf(id)));

		final ResponseOutput<DiscussionDTO> response = new ResponseOutput<DiscussionDTO>(discussionService.findDiscussion(id));
		logger.info(StringUtils.join("response payload for findDiscussion -> ", response));

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}


}
