package br.com.sicredi.cooperative.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.sicredi.cooperative.entity.Discussion;

@Repository
public interface DiscussionRepository extends CrudRepository<Discussion, Long> {


}
