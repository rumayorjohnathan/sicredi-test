package br.com.sicredi.cooperative.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.sicredi.cooperative.entity.Partner;

@Repository
public interface PartnerRepository extends CrudRepository<Partner, Long> {

	Optional<Partner> findByCpf(String cpf);


}
