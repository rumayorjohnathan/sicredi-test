package br.com.sicredi.cooperative.service;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.sicredi.cooperative.client.CpfClient;
import br.com.sicredi.cooperative.client.output.CpfOutput;
import br.com.sicredi.cooperative.dto.KeyDTO;
import br.com.sicredi.cooperative.entity.Partner;
import br.com.sicredi.cooperative.repository.PartnerRepository;
import br.com.sicredi.cooperative.util.SicrediBusinessException;

@Service
public class PartnerService {

	@Autowired
	private Logger logger;

	@Autowired
	private PartnerRepository partnerRepository;

	@Autowired
	private CpfClient cpfClient;

	public KeyDTO checkPartner(String cpf) {
		final CpfOutput cpfOutput = cpfClient.checkCpfPermission(cpf);

		if (cpfOutput.isUnableToVote()) {
			throw new SicrediBusinessException(HttpStatus.FORBIDDEN, "Infelizmente o CPF de input não possui credenciais de votação!");
		}

		final Optional<Partner> partnerOpt = partnerRepository.findByCpf(cpf);

		if (!partnerOpt.isPresent()) {
			final Partner partner = new Partner(null, cpf);
			partnerRepository.save(partner);
			logger.info(StringUtils.join("Registered partner into database -> ", partner));
			return new KeyDTO(partner.getId());
		}

		logger.info(StringUtils.join("Partner alrealdy registered into database -> ", partnerOpt.get()));
		return new KeyDTO(partnerOpt.get().getId());
	}
}
