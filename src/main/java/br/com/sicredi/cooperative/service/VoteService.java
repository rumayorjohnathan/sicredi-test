package br.com.sicredi.cooperative.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.sicredi.cooperative.dto.DiscussionDTO;
import br.com.sicredi.cooperative.dto.KeyDTO;
import br.com.sicredi.cooperative.dto.SessionDTO;
import br.com.sicredi.cooperative.dto.VoteDTO;
import br.com.sicredi.cooperative.entity.Vote;
import br.com.sicredi.cooperative.repository.VoteRepository;
import br.com.sicredi.cooperative.util.SicrediBusinessException;

@Service
public class VoteService {

	@Autowired
	private Logger logger;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private SessionService sessionService;

	@Autowired
	private DiscussionService discussionService;

	@Autowired
	private PartnerService partnerService;

	@Autowired
	private VoteRepository voteRepository;

	public KeyDTO createVote(Long discussionId, VoteDTO voteDto) {
		logger.info(StringUtils.join("createVote for input discussionId -> ", discussionId, " | ", voteDto));

		final KeyDTO keyDto = partnerService.checkPartner(voteDto.getCpf());
		final DiscussionDTO discussionDto = discussionService.findDiscussion(discussionId);
		final SessionDTO sessionDto = sessionService.findByDiscussionId(discussionDto.getId());

		if (sessionDto == null || !sessionDto.getOpen()) {
			logger.info(StringUtils.join("createVote for input discussionId expired or inexistent -> ", discussionId, " | ", sessionDto));
			throw new SicrediBusinessException(HttpStatus.FORBIDDEN, "Discussion not able to be voted.");
		}

		final Optional<Vote> voteOpt  = voteRepository.findBySessionIdAndPartnerId(sessionDto.getId(), keyDto.getId());

		if (voteOpt.isPresent()) {
			logger.info(StringUtils.join("createVote for input discussionId already registered -> ", discussionId, " | ", voteOpt.get()));
			throw new SicrediBusinessException(HttpStatus.CONFLICT, "Cooperado já possui voto registrado na sessão de votação.");
		}

		final Vote vote = new Vote();
		vote.setPartnerId(keyDto.getId());
		vote.setSessionId(sessionDto.getId());
		vote.setValue(voteDto.getValue());
		vote.setCreation(System.currentTimeMillis());

		voteRepository.save(vote);

		logger.info(StringUtils.join("createVote for input discussionId registered -> ", discussionId, " | ", vote));

		return new KeyDTO(vote.getId());
	}

	public List<VoteDTO> findVotesBySessionId(Long sessionId) {
		logger.info(StringUtils.join("findVotesBySessionId for input sessionId -> ", sessionId));

		final List<Vote> votes = voteRepository.findBySessionId(sessionId);
		logger.info(StringUtils.join("findVotesBySessionId for input sessionId -> ", sessionId, " | ", votes));

		return votes.stream().map(v -> modelMapper.map(v, VoteDTO.class)).collect(Collectors.toList());
	}
}
