package br.com.sicredi.cooperative.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import br.com.sicredi.cooperative.dto.DiscussionDTO;
import br.com.sicredi.cooperative.dto.KeyDTO;
import br.com.sicredi.cooperative.dto.SessionDTO;
import br.com.sicredi.cooperative.dto.SessionResultDTO;
import br.com.sicredi.cooperative.dto.VoteDTO;
import br.com.sicredi.cooperative.entity.Session;
import br.com.sicredi.cooperative.repository.SessionRepository;
import br.com.sicredi.cooperative.util.SicrediBusinessException;

@Service
public class SessionService {

	@Autowired
	private Logger logger;

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private DiscussionService discussionService;

	@Autowired
	private VoteService voteService;

	@Autowired
	private SessionRepository sessionRepository;

	public KeyDTO createSession(Long discussionId, SessionDTO sessionDto) {
		logger.info(StringUtils.join("createSession for input discussionId -> ", discussionId, " | ", sessionDto));

		final DiscussionDTO discussion = discussionService.findDiscussion(discussionId);
		final Optional<Session> sessionOpt = sessionRepository.findByDiscussionId(discussionId);

		if (sessionOpt.isPresent()) {
			logger.info(StringUtils.join("createSession conflict for input discussionId -> ", discussionId, " | ", sessionOpt.get()));
			throw new SicrediBusinessException(HttpStatus.CONFLICT, "Pauta já possui uma sessão de votos cadastrada.");
		}

		final Session sessionEntity = mapper.map(sessionDto, Session.class);
		final Long deadline = LocalDateTime.now().plusMinutes(1L).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
	
		sessionEntity.setCreated(System.currentTimeMillis());
		sessionEntity.setDeadline(sessionDto.getDeadline() != null ? sessionDto.getDeadline() : deadline);
		sessionEntity.setDiscussionId(discussion.getId());
		sessionEntity.setOpen(Boolean.TRUE);

		sessionRepository.save(sessionEntity);

		logger.info(StringUtils.join("createSession created for input discussionId -> ", discussionId, " | ", sessionEntity));

		return new KeyDTO(sessionEntity.getId());
	}

	public SessionDTO findByDiscussionId(Long discussionId) {
		logger.info(StringUtils.join("findByDiscussionId for input discussionId -> ", discussionId));

		final Optional<Session> sessionOpt = sessionRepository.findByDiscussionId(discussionId);
		
		if (!sessionOpt.isPresent()) {
			logger.info(StringUtils.join("findByDiscussionId not found for input discussionId -> ", discussionId));
			throw new SicrediBusinessException(HttpStatus.NOT_FOUND, "Nenhuma sessão existente para esta Pauta");
		}

		final Session session = sessionOpt.get();
		logger.info(StringUtils.join("findByDiscussionId found for input discussionId -> ", discussionId, " | ", session));
		
		return mapper.map(session, SessionDTO.class);

	}

	@Nullable
	public SessionDTO findSessionSummaryByDiscussionId(Long discussionId) {
		logger.info(StringUtils.join("findSessionSummaryByDiscussionId for input discussionId -> ", discussionId));

		final SessionDTO sessionDto = findByDiscussionId(discussionId);

		final List<VoteDTO> votesDto = voteService.findVotesBySessionId(sessionDto.getId());
		final Map<Boolean, List<VoteDTO>> checkingResult = votesDto.stream().collect(Collectors.groupingBy(VoteDTO::getValue));
		final List<VoteDTO> favorableVotes = checkingResult.containsKey(Boolean.TRUE) ? checkingResult.get(Boolean.TRUE) : Arrays.asList();
		final List<VoteDTO> againstVotes = checkingResult.containsKey(Boolean.FALSE) ? checkingResult.get(Boolean.FALSE) : Arrays.asList();		

		final Integer totalVotes = votesDto.size();
		final Integer favorableVotesCounter = favorableVotes.size();
		final Double favorablePercent = (Double) (totalVotes == 0 ? 0D : favorableVotesCounter / Double.valueOf(totalVotes)) * 100;
		final Integer againstVotesCounter = againstVotes.size();
		final Double againstPercent = (Double) (totalVotes == 0 ? 0D : againstVotesCounter / Double.valueOf(totalVotes)) * 100;

		final SessionResultDTO sessionResultDto = new SessionResultDTO(
				favorableVotesCounter, 
				favorablePercent, 
				againstVotesCounter, 
				againstPercent, 
				totalVotes);
		
		sessionDto.setSessionResult(sessionResultDto);
		
		return sessionDto;
	}

	public List<Long> closeSessions() {
		final Long deadline = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
		logger.debug(StringUtils.join("findByDeadlineAndOpen for input deadline and open -> ", deadline));

		final List<Session> sessionsEntity = sessionRepository.findByOpen(Boolean.TRUE);
		final List<Session> sessionsToClose = sessionsEntity.stream().filter(s -> s.getDeadline() < deadline).collect(Collectors.toList());

		logger.debug(StringUtils.join("findByDeadlineAndOpen for input deadline and open -> ", sessionsEntity));

		sessionsToClose.stream().forEach(s -> {
			s.setOpen(Boolean.FALSE);
			logger.info(StringUtils.join("findByDeadlineAndOpen close session -> ", s));
			sessionRepository.save(s);
		});
		
		logger.debug(StringUtils.join("findByDeadlineAndOpen for input deadline and open -> ", sessionsEntity));

		return sessionsToClose.stream().map(Session::getDiscussionId).collect(Collectors.toList());

	}
	

}
