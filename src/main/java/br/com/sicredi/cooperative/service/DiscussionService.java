package br.com.sicredi.cooperative.service;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.sicredi.cooperative.dto.DiscussionDTO;
import br.com.sicredi.cooperative.dto.KeyDTO;
import br.com.sicredi.cooperative.entity.Discussion;
import br.com.sicredi.cooperative.repository.DiscussionRepository;
import br.com.sicredi.cooperative.util.SicrediBusinessException;

@Service
public class DiscussionService {

	@Autowired
	private Logger logger;

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private DiscussionRepository discussionRepository;

	public DiscussionDTO findDiscussion(Long id) {
		logger.info(StringUtils.join("findDiscussion for input id -> ", id));

		final Discussion discussionEntity = findById(id);
		logger.info(StringUtils.join("findDiscussion for input id is present -> ", id, discussionEntity));

		final DiscussionDTO discussionDto = mapper.map(discussionEntity, DiscussionDTO.class);		
		logger.info(StringUtils.join("findDiscussion returning discussionDto -> ", id, discussionDto));

		return discussionDto;
	}

	public KeyDTO createDiscussion(DiscussionDTO discussionDto) {
		logger.info(StringUtils.join("createDiscussion for input -> ", discussionDto));

		final Discussion discussionEntity = mapper.map(discussionDto, Discussion.class);
		discussionEntity.setCreated(System.currentTimeMillis());

		logger.info(StringUtils.join("createDiscussion for input ready to persist -> ", discussionDto));
		discussionRepository.save(discussionEntity);
		logger.info(StringUtils.join("createDiscussion for input persisted -> ", discussionDto));

		return new KeyDTO(discussionEntity.getId());
	}

	private Discussion findById(Long id) {
		logger.info(StringUtils.join("findById for input -> ", id));
		final Optional<Discussion> discussionOpt = discussionRepository.findById(id);

		if (!discussionOpt.isPresent()) {
			logger.info(StringUtils.join("findById for input id is not present -> ", id));
			throw new SicrediBusinessException(HttpStatus.NOT_FOUND, "Pauta não encontrada.");
		}

		final Discussion discussion = discussionOpt.get();
		logger.info(StringUtils.join("findById for input is present -> ", discussion));
		return discussion;
	}

}
