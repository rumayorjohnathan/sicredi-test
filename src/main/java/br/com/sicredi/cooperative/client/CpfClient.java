package br.com.sicredi.cooperative.client;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import br.com.sicredi.cooperative.client.output.CpfOutput;
import br.com.sicredi.cooperative.util.SicrediBusinessException;

@Service
public class CpfClient {

	@Autowired
	private Logger logger;

	@Autowired
	private RestTemplate restTemplate;

	public CpfOutput checkCpfPermission(String cpf) {
		logger.info(StringUtils.join("checkCpfPermission for input cpf -> ", cpf));

		final ResponseEntity<CpfOutput> response;
		try {
			final String uri = StringUtils.join("https://user-info.herokuapp.com/users/", cpf);
			response = restTemplate.getForEntity(uri, CpfOutput.class);

			final CpfOutput cpfOutput = response.getBody();
			logger.info(StringUtils.join("retrieved cpfoutput for input cpf -> ", cpf, " | ", cpfOutput));

			return cpfOutput;
		} catch (HttpClientErrorException | HttpServerErrorException e) {
			logger.error(StringUtils.join("error retrieving cpfoutput for input cpf -> ", cpf, " | ", e));

			throw new SicrediBusinessException(e.getStatusCode(), e.getResponseBodyAsString());
		}
	}
}
