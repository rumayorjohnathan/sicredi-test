package br.com.sicredi.cooperative.util;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;


public class CPFDeserializer extends JsonDeserializer<String> {

	@Override
	public String deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		final String maskedCpf = p.getText();

		if (StringUtils.isBlank(maskedCpf)) {
			return null;
		}

		final String unmaskedCpf = maskedCpf.replaceAll("\\D+", "");

		return StringUtils.leftPad(unmaskedCpf, 11, '0');
	}

}
