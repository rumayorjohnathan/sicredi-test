package br.com.sicredi.cooperative.util;

import org.springframework.http.HttpStatus;

public class SicrediBusinessException extends RuntimeException {

	private static final long serialVersionUID = -6125298283326234461L;

	private HttpStatus httpStatus;

	public SicrediBusinessException() {
		super();
	}

	public SicrediBusinessException(HttpStatus httpStatus, String message) {
		super(message);
		this.httpStatus = httpStatus;
	}

	public SicrediBusinessException(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	public SicrediBusinessException(String message) {
		super(message);
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}


}
