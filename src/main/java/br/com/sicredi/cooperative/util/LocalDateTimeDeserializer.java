package br.com.sicredi.cooperative.util;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

	@Override
	public LocalDateTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		final String value = p.getText();
		if (StringUtils.isBlank(value)) {
			return null;
		}
		try {
			final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			final LocalDateTime dateTime = LocalDateTime.parse(value, formatter);
			return dateTime;
		} catch (Exception e) {
			throw new SicrediBusinessException(HttpStatus.UNPROCESSABLE_ENTITY, "Padrão da data deve ser yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		}

	}

}
