package br.com.sicredi.cooperative.dto;

import java.io.Serializable;

public class SessionResultDTO implements Serializable {

	private static final long serialVersionUID = -7732894758992192438L;

	private Integer favorableVotes;
	private Double favorablePercent;
	private Integer againstVotes;
	private Double againstPercent;
	private Integer totalVotes;

	public SessionResultDTO() {
		super();
	}

	public SessionResultDTO(Integer favorableVotes, Double favorablePercent, Integer againstVotes, Double againstPercent,
			Integer totalVotes) {
		this();
		this.favorableVotes = favorableVotes;
		this.favorablePercent = favorablePercent;
		this.againstVotes = againstVotes;
		this.againstPercent = againstPercent;
		this.totalVotes = totalVotes;
	}

	public Integer getFavorableVotes() {
		return favorableVotes;
	}

	public void setFavorableVotes(Integer favorableVotes) {
		this.favorableVotes = favorableVotes;
	}

	public Double getFavorablePercent() {
		return favorablePercent;
	}

	public void setFavorablePercent(Double favorablePercent) {
		this.favorablePercent = favorablePercent;
	}

	public Integer getAgainstVotes() {
		return againstVotes;
	}

	public void setAgainstVotes(Integer againstVotes) {
		this.againstVotes = againstVotes;
	}

	public Double getAgainstPercent() {
		return againstPercent;
	}

	public void setAgainstPercent(Double againstPercent) {
		this.againstPercent = againstPercent;
	}

	public Integer getTotalVotes() {
		return totalVotes;
	}

	public void setTotalVotes(Integer totalVotes) {
		this.totalVotes = totalVotes;
	}

	@Override
	public String toString() {
		return "SessionResultDTO [favorableVotes=" + favorableVotes + ", favorablePercent=" + favorablePercent
				+ ", againstVotes=" + againstVotes + ", againstPercent=" + againstPercent + ", totalVotes=" + totalVotes
				+ "]";
	}

}
