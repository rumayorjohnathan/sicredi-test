package br.com.sicredi.cooperative.dto;

import java.io.Serializable;

public class VoteDTO implements Serializable {

	private static final long serialVersionUID = -1227454291113195465L;

	private Long id;
	private Long discussionId;
	private Long partnerId;
	private Boolean value;
	private String cpf;

	public VoteDTO() {
		super();
	}

	public VoteDTO(Long id, Long discussionId, Long partnerId, Boolean value, String cpf) {
		this();
		this.id = id;
		this.discussionId = discussionId;
		this.partnerId = partnerId;
		this.value = value;
		this.cpf = cpf;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDiscussionId() {
		return discussionId;
	}

	public void setDiscussionId(Long discussionId) {
		this.discussionId = discussionId;
	}

	public Long getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(Long partnerId) {
		this.partnerId = partnerId;
	}

	public Boolean getValue() {
		return value;
	}

	public void setValue(Boolean value) {
		this.value = value;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	@Override
	public String toString() {
		return "VoteDTO [id=" + id + ", discussionId=" + discussionId + ", partnerId=" + partnerId + ", value=" + value
				+ ", cpf=" + cpf + "]";
	}


}
