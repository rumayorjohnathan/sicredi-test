package br.com.sicredi.cooperative.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;

public class KeyDTO implements Serializable {

	private static final long serialVersionUID = 5310271867472575858L;

	private Long id;

	public KeyDTO() {
		super();
	}

	public KeyDTO(Long id) {
		this();
		this.id = id;
	}

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "KeyDTO [id=" + id + "]";
	}

}
