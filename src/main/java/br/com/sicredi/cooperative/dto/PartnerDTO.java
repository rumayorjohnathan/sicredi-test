package br.com.sicredi.cooperative.dto;

import java.io.Serializable;

public class PartnerDTO implements Serializable {

	private static final long serialVersionUID = 6180672352916918103L;

	private Long id;
	private String cpf;

	public PartnerDTO() {
		super();
	}

	public PartnerDTO(Long id, String cpf) {
		super();
		this.id = id;
		this.cpf = cpf;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	@Override
	public String toString() {
		return "PartnerDTO [id=" + id + ", cpf=" + cpf + "]";
	}

}
