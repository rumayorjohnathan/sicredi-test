package br.com.sicredi.cooperative.dto;

import java.io.Serializable;

public class DiscussionDTO implements Serializable {

	private static final long serialVersionUID = 7353647296434551492L;

	private Long id;
	private String title;
	private String description;
	private Long created;

	public DiscussionDTO() {
		super();
	}

	public DiscussionDTO(Long id, String title, String description, Long created) {
		this();
		this.id = id;
		this.title = title;
		this.description = description;
		this.created = created;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}

	@Override
	public String toString() {
		return "DiscussionDTO [id=" + id + ", title=" + title + ", description=" + description + ", created=" + created
				+ "]";
	}

}
