package br.com.sicredi.cooperative.schedule;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import br.com.sicredi.cooperative.client.RabbitClient;
import br.com.sicredi.cooperative.dto.SessionDTO;
import br.com.sicredi.cooperative.service.SessionService;

@Service
public class SessionWatcher {

	@Autowired
	private SessionService sessionService;

	@Autowired
	private RabbitClient rabbitClient;

	@Scheduled(cron = "*/3 * * * * *")
	public void verifyExpiredSessions() {
		final List<Long> closedIds = sessionService.closeSessions();
		
		closedIds.stream().forEach(i -> {
			final SessionDTO sessionDto = sessionService.findSessionSummaryByDiscussionId(i);
			rabbitClient.send(sessionDto);
		});
	}
 
}
