package br.com.sicredi.cooperative.api.v1;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.sicredi.cooperative.api.v1.input.VoteInput;
import br.com.sicredi.cooperative.client.CpfClient;
import br.com.sicredi.cooperative.client.output.CpfOutput;
import br.com.sicredi.cooperative.entity.Discussion;
import br.com.sicredi.cooperative.entity.Partner;
import br.com.sicredi.cooperative.entity.Session;
import br.com.sicredi.cooperative.entity.Vote;
import br.com.sicredi.cooperative.repository.DiscussionRepository;
import br.com.sicredi.cooperative.repository.PartnerRepository;
import br.com.sicredi.cooperative.repository.SessionRepository;
import br.com.sicredi.cooperative.repository.VoteRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class SessionVoteApiTest {

	@MockBean
	private DiscussionRepository discussionRepository;

	@MockBean
	private SessionRepository sessionRepository;

	@MockBean
	private VoteRepository voteRepository;

	@MockBean
	private PartnerRepository partnerRepository;

	@MockBean
	private CpfClient cpfClient;

	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;

	private final String cpf = "01123362050";

	@Before
	public void setUp() {
		when(discussionRepository.findById(eq(1L))).thenReturn(Optional.of(new Discussion(1L, "Test", "Test ok", 12345L)));
		when(sessionRepository.findByDiscussionId(eq(1L))).thenReturn(Optional.of(new Session(1L, 1L, null, null, Boolean.TRUE)));
		when(partnerRepository.findByCpf(eq(cpf))).thenReturn(Optional.of(new Partner(1L, cpf)));
		when(voteRepository.findBySessionIdAndPartnerId(eq(1L), eq(1L))).thenReturn(Optional.empty());
		when(voteRepository.save(any(Vote.class))).thenReturn(new Vote());
		when(cpfClient.checkCpfPermission(cpf)).thenReturn(new CpfOutput(CpfOutput.CpfStatus.ABLE_TO_VOTE));
	}
	@Test
	public void createVoteOkTest() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/v1/discussions/1/votes")
				.content(objectMapper.writeValueAsString(new VoteInput(Boolean.TRUE, cpf)))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
	}

	@Test
	public void createVoteConflictTest() throws Exception {
		when(voteRepository.findBySessionIdAndPartnerId(eq(1L), eq(1L))).thenReturn(Optional.of(new Vote()));

		mvc.perform(MockMvcRequestBuilders.post("/v1/discussions/1/votes")
				.content(objectMapper.writeValueAsString(new VoteInput(Boolean.TRUE, cpf)))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isConflict());
	}

	@Test
	public void createVoteCpfForbiddenTest() throws Exception {
		when(cpfClient.checkCpfPermission(cpf)).thenReturn(new CpfOutput(CpfOutput.CpfStatus.UNABLE_TO_VOTE));

		mvc.perform(MockMvcRequestBuilders.post("/v1/discussions/1/votes")
				.content(objectMapper.writeValueAsString(new VoteInput(Boolean.TRUE, cpf)))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isForbidden());
	}

	@Test
	public void createVoteClosedSessionTest() throws Exception {
		when(sessionRepository.findByDiscussionId(eq(1L))).thenReturn(Optional.of(new Session(1L, 1L, null, null, Boolean.FALSE)));

		mvc.perform(MockMvcRequestBuilders.post("/v1/discussions/1/votes")
				.content(objectMapper.writeValueAsString(new VoteInput(Boolean.TRUE, cpf)))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isForbidden());
	}

	@Test
	public void createVoteUnprocessableEntityTest() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/v1/discussions/1/votes")
				.content(objectMapper.writeValueAsString(new VoteInput()))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isUnprocessableEntity());

	}
}
