package br.com.sicredi.cooperative.api.v1;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.sicredi.cooperative.api.v1.input.SessionInput;
import br.com.sicredi.cooperative.entity.Discussion;
import br.com.sicredi.cooperative.entity.Session;
import br.com.sicredi.cooperative.repository.DiscussionRepository;
import br.com.sicredi.cooperative.repository.SessionRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class DiscussionSessionApiTest {

	@MockBean
	private DiscussionRepository discussionRepository;

	@MockBean
	private SessionRepository sessionRepository;

	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void createSessionOkTest() throws Exception {
		when(discussionRepository.findById(eq(1L))).thenReturn(Optional.of(new Discussion(1L, "Test", "Test ok", 12345L)));
		when(sessionRepository.findByDiscussionId(1L)).thenReturn(Optional.empty());

		mvc.perform(MockMvcRequestBuilders.post("/v1/discussions/1/sessions")
				.content(objectMapper.writeValueAsString(new SessionInput()))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
	}

	@Test
	public void createSessionConflictTest() throws Exception {
		when(discussionRepository.findById(eq(1L))).thenReturn(Optional.of(new Discussion(1L, "Test", "Test ok", 12345L)));
		when(sessionRepository.findByDiscussionId(1L)).thenReturn(Optional.of(new Session()));

		mvc.perform(MockMvcRequestBuilders.post("/v1/discussions/1/sessions")
				.content(objectMapper.writeValueAsString(new SessionInput()))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isConflict());
	}

	@Test
	public void findSessionOkTest() throws Exception {
		when(sessionRepository.findByDiscussionId(1L)).thenReturn(Optional.of(new Session(1L, 1L, null, null, Boolean.TRUE)));
	
		mvc.perform(MockMvcRequestBuilders.get("/v1/discussions/1/sessions")
				.content(objectMapper.writeValueAsString(new SessionInput()))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	public void findSessionNotTest() throws Exception {
		when(sessionRepository.findByDiscussionId(1L)).thenReturn(Optional.empty());
	
		mvc.perform(MockMvcRequestBuilders.get("/v1/discussions/1/sessions")
				.content(objectMapper.writeValueAsString(new SessionInput()))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}
}
