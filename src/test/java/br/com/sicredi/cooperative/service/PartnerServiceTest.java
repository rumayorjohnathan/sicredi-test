package br.com.sicredi.cooperative.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.sicredi.cooperative.client.CpfClient;
import br.com.sicredi.cooperative.client.output.CpfOutput;
import br.com.sicredi.cooperative.entity.Partner;
import br.com.sicredi.cooperative.repository.PartnerRepository;
import br.com.sicredi.cooperative.util.SicrediBusinessException;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class PartnerServiceTest {

	@MockBean
	private PartnerRepository partnerRepository;

	@MockBean
	private CpfClient cpfClient;

	@Autowired
	private PartnerService partnerService;

	final String cpf = "01123362050";

	@Test
	public void checkPartnerInexistentTest() {
		when(cpfClient.checkCpfPermission(eq(cpf))).thenReturn(new CpfOutput(CpfOutput.CpfStatus.ABLE_TO_VOTE));
		when(partnerRepository.findByCpf(eq(cpf))).thenReturn(Optional.empty());

		partnerService.checkPartner(cpf);

		verify(partnerRepository, times(1)).findByCpf(eq(cpf));
		verify(cpfClient, times(1)).checkCpfPermission(eq(cpf));
		verify(partnerRepository, times(1)).save(any(Partner.class));
	}

	@Test
	public void checkPartnerExistentTest() {
		when(cpfClient.checkCpfPermission(eq(cpf))).thenReturn(new CpfOutput(CpfOutput.CpfStatus.ABLE_TO_VOTE));
		when(partnerRepository.findByCpf(eq(cpf))).thenReturn(Optional.of(new Partner(1L, cpf)));

		partnerService.checkPartner(cpf);

		verify(partnerRepository, times(1)).findByCpf(eq(cpf));
		verify(cpfClient, times(1)).checkCpfPermission(eq(cpf));
		verify(partnerRepository, times(0)).save(any(Partner.class));
	}

	@Test
	public void checkPartnerForbiddenTest() {
		when(cpfClient.checkCpfPermission(eq(cpf))).thenReturn(new CpfOutput(CpfOutput.CpfStatus.UNABLE_TO_VOTE));

		try {
			partnerService.checkPartner(cpf);
		} catch (SicrediBusinessException sbe) {
			verify(partnerRepository, times(0)).findByCpf(eq(cpf));
			verify(cpfClient, times(1)).checkCpfPermission(eq(cpf));
			verify(partnerRepository, times(0)).save(any(Partner.class));
			assertEquals(HttpStatus.FORBIDDEN, sbe.getHttpStatus());
		}
	}


}
