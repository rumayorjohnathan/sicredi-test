

## **Desafio técnico de Back-end Sicredi**

Desafio de implementação imposto aos candidatos a integrar o time de back-end do Sicredi em Porto Alegre. Desafio de pode encontrado neste link [aqui](https://drive.google.com/open?id=0Bzav_TVrRBoJWl9keTBXVURuUXVSX3dMTmNHMDNhQlVNNjk0).

Sobre as tecnologias: 
Estamos utilizando **micro-serviços** rodando sob **Spring Boot**, pois possuem grande facilidade e funcionalidades voltadas para integrações, múltiplos perfis e grande escalabilidade, além de, possuírem plug-ins que provém formas de integrações via cloud e manutenibilidade dos mesmos. Alguns desses foram utilizados durante desenvolvimento do projeto:

 1. spring-boot-starter-web
 2. spring-boot-starter-actuator
 3. spring-boot-starter-amqp
 4. spring-data-redis
 5. spring-boot-starter-test

Também optei pela utilização de Redis para gerenciamento dos dados visando a fraca tipagem e possibilidade de volatilidade da base, uma vez que o restart do micro-serviço não pode implicar na perda dos dados. Outra tecnologia periférica a implementação é o RabbitMQ para atendimento de uma das tarefas bônus explicitadas, escolhida pela familiaridade de experiências anteriores e, também, pela fácil integração ao Java implementado.
A implementação do código está disponível neste Bitbucket uma Docker image clicando [aqui](https://hub.docker.com/r/johnathanrumayor/cooperative-vote-service):

 - Tarefa Bônus 1: [Impl_1](https://drive.google.com/file/d/1QTL2T5Mwc0PJ-cYb2YJ58498zi0PcMsl/view?usp=sharing) [Impl_2](https://drive.google.com/file/d/1ViL9BbUojmXcs1AQmVmuWW9TdOeaTYF6/view?usp=sharing)
 - Tarefa Bônus 2: [Impl_1](https://drive.google.com/file/d/1MgYaswBY-_K8pmE7a95ay_A_OLAZc2pf/view?usp=sharing) [Impl_2](https://drive.google.com/file/d/19WopLtDJNNm3q6ceSCYfxYuhl5TBLBN6/view?usp=sharing)
 - Tarefa Bonus 3: [Resultados dos Testes utilizando JMeter](https://bitbucket.org/rumayorjohnathan/sicredi-test/src/dev/performance-tests/)
 - Tarefa Bônus 4: Utilizando-se da nomenclatura dos pacotes Java, podemos tranquilamente criar diversas versões de APIs abaixo do pacote pai de api. [Exemplificando](https://drive.google.com/file/d/1v3slztLsxNoB3b_OaniHnAq9b89HZj5H/view?usp=sharing).

## How2Test:

Ambiente para executar/testar localmente:
 - Possuir Java 8 e Gradle 5 instalado em seu ambiente.
  - Possuir docker e docker-compose disponível em seu ambiente.
  - Importante que as portas *8080*, *6379*, *5672* e *15672* não estejam ocupadas.

Passo 1:

 1. Clonar o repositório neste [link](https://bitbucket.org/rumayorjohnathan/sicredi-test/)
 2. Ir até o diretório clonado e executar o seguinte comando:
> docker-compose -f docker-compose.yml up --build 

3. No mesmo diretório, executar o próximo comando:
> gradle build && java -jar build/libs/cooperative-vote-service-0.0.1.jar

4. Seu ambiente está disponível para ser testado.

Há a configuração de um Swagger para interação visual com a API criada e fácil entendimento dos contratos da aplicação. Disponível na porta :8080/swagger-ui.html.

*Fica disponível também um Dockerfile de exemplo caso seja útil.*
