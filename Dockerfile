FROM openjdk:8-jdk-alpine

LABEL maintainer="rumayorjohnathan@gmail.com"

COPY ./build/libs/cooperative-vote-service-0.0.1.jar /usr/app/

WORKDIR /usr/app

EXPOSE 8080

RUN sh -c 'touch cooperative-vote-service-0.0.1.jar'

ENTRYPOINT ["java","-jar","cooperative-vote-service-0.0.1.jar"]
